<?php
return array(
    'router' => array(
        'routes' => array(
            'property-app.rest.property' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/property[/:property_id]',
                    'defaults' => array(
                        'controller' => 'PropertyApp\\V1\\Rest\\Property\\Controller',
                    ),
                ),
            ),
            'property-app.rest.agent' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agent[/:agent_id]',
                    'defaults' => array(
                        'controller' => 'PropertyApp\\V1\\Rest\\Agent\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'property-app.rest.property',
            1 => 'property-app.rest.agent',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'PropertyApp\\V1\\Rest\\Property\\Resource' => 'PropertyApp\\V1\\Rest\\Property\\ResourceFactory',
            'PropertyApp\\V1\\Rest\\Agent\\AgentResource' => 'PropertyApp\\V1\\Rest\\Agent\\AgentResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'PropertyApp\\V1\\Rest\\Property\\Controller' => array(
            'listener' => 'PropertyApp\\V1\\Rest\\Property\\Resource',
            'route_name' => 'property-app.rest.property',
            'route_identifier_name' => 'property_id',
            'collection_name' => 'property',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'PropertyApp\\V1\\Rest\\Property\\Entity',
            'collection_class' => 'PropertyApp\\V1\\Rest\\Property\\Collection',
            'service_name' => 'Property',
        ),
        'PropertyApp\\V1\\Rest\\Agent\\Controller' => array(
            'listener' => 'PropertyApp\\V1\\Rest\\Agent\\AgentResource',
            'route_name' => 'property-app.rest.agent',
            'route_identifier_name' => 'agent_id',
            'collection_name' => 'agent',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'PropertyApp\\V1\\Rest\\Agent\\AgentEntity',
            'collection_class' => 'PropertyApp\\V1\\Rest\\Agent\\AgentCollection',
            'service_name' => 'Agent',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'PropertyApp\\V1\\Rest\\Property\\Controller' => 'HalJson',
            'PropertyApp\\V1\\Rest\\Agent\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'PropertyApp\\V1\\Rest\\Property\\Controller' => array(
                0 => 'application/vnd.property-app.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'PropertyApp\\V1\\Rest\\Agent\\Controller' => array(
                0 => 'application/vnd.property-app.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'PropertyApp\\V1\\Rest\\Property\\Controller' => array(
                0 => 'application/vnd.property-app.v1+json',
                1 => 'application/json',
            ),
            'PropertyApp\\V1\\Rest\\Agent\\Controller' => array(
                0 => 'application/vnd.property-app.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'PropertyApp\\V1\\Rest\\Property\\Entity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'property-app.rest.property',
                'route_identifier_name' => 'property_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'PropertyApp\\V1\\Rest\\Property\\Collection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'property-app.rest.property',
                'route_identifier_name' => 'property_id',
                'is_collection' => true,
            ),
            'PropertyApp\\V1\\Rest\\Agent\\AgentEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'property-app.rest.agent',
                'route_identifier_name' => 'agent_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'PropertyApp\\V1\\Rest\\Agent\\AgentCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'property-app.rest.agent',
                'route_identifier_name' => 'agent_id',
                'is_collection' => true,
            ),
        ),
    ),
);
