<?php

namespace PropertyApp\V1\Rest\Property;

use Zend\ServiceManager\ServiceLocatorInterface;
use ZF\ApiProblem\Exception\DomainException;

class TableGatewayFactory
{
    /**
     * @param $services
     * @return TableGateway
     * @throws \ZF\ApiProblem\Exception\DomainException
     */
    public function __invoke(ServiceLocatorInterface $services)
    {
        $db    = 'Db';
        $table = 'property';

        /**
         * Allow alternative configuration override e.g. for testing
         */
        if ($services->has('config')) {
            $config = $services->get('config');
            switch (isset($config['property'])) {
                case true:
                    $config = $config['property'];

                    if (array_key_exists('db', $config) && !empty($config['db'])) {
                        $db = $config['db'];
                    }

                    if (array_key_exists('table', $config) && !empty($config['table'])) {
                        $table = $config['table'];
                    }
                    break;
                case false:
                default:
                    break;
            }
        }

        if (! $services->has($db)) {
            throw new DomainException(
                sprintf(
                    'Unable to create TableGateway due to missing "%s" service',
                    $db
                )
            );
        }

        return new TableGateway($table, $services->get($db));
    }
}