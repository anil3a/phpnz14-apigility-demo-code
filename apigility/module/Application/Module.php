<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Db' => function ($sm) {
                    return new Adapter(
                        array(
                            'dsn' => 'mysql:dbname=property_app;host=127.0.0.1',
                            'username' => 'root',
                            'password' => 'root',
                            'driver' => 'Pdo_Mysql',
                            'driver_options' => array(
                                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                            ),
                        )
                    );
                },
            ),
        );
    }
}
